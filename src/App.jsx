import {BrowserRouter, Route, Routes} from "react-router-dom";
import Login from './pages/Login';
import Admin from "./pages/Admin";
import AuthLayout from './layouts/AuthLayout';
import {UsuariosProvider} from "./context/UsuariosProvider";
import './index.css';
function App() {

    return (
        <BrowserRouter>
            <UsuariosProvider>
                <Routes>
                    <Route path="/" element={<AuthLayout/>}> 
                        <Route index element={<Login/>}/>
                        <Route path="admin" element={<Admin/>}/>
                        <Route path="olvide-password" element={<></>}/>
                        <Route path="olvide-password/:token" element={<></>}/>
                        <Route path="confirmar/:id" element={<></>}/> 
                    </Route>
                </Routes>
            </UsuariosProvider>
        </BrowserRouter>
    );
}

export default App
