import { Link } from "react-router-dom"

const Header = () => {
    return (
        <header className="px-4 py-5 border-b">
            <div className="md:flex md:justify-between">
                <h2 className="text-4xl text-sky-600 font-black text-center mb-5 md:mb-0 ">Admin Users</h2>

                <div className="flex flex-col md:flex-row items-center gap-4">
                    {/* <button
                        type="button"
                        className="font-bold uppercase"
                        
                    >Buscar Proyecto</button>
                    <Link
                        to="/proyectos"
                        className="font-bold uppercase"
                        >Proyectos</Link>

                    <button
                        type="button"
                        className="bg-sky-600 text-white text-sm uppercase font-bold p-3 rounded-md"
                    >Cerrar Sesión</button> */}
                </div>
            </div>
        </header>
    )
}

export default Header