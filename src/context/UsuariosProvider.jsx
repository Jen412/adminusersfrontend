import { createContext, useState, useEffect } from "react";
import clienteAxios from "../config/clienteAxios";

const UsuariosContext = createContext();

const UsuariosProvider = ({children}) => {
    const [usuarios, setUsuarios] = useState({});

    useEffect(() => {
        const obtenerUsuarios = async () => {
            try {
                const config={
                    headers:{
                        "Content-Type": "application/json"
                    }
                }
                const {data}= await clienteAxios("/users",config);
                setUsuarios(data);
            } catch (error) {
                console.log(error);
            }
        }
        return () => {obtenerUsuarios()};
    }, []); 

    return (
        <UsuariosContext.Provider
            value={{
                usuarios
            }}
        >{children}
        </UsuariosContext.Provider>
    );
}

export {
    UsuariosProvider,
}

export default UsuariosContext