import { Outlet } from "react-router-dom"
import Header from "../components/Header"
const AuthLayout = () => {
    return (
        <>
            <Header/>
            <main className="container mx-auto mt-5 md:mt-20 p-5 md:flex md:justify-center">
                <Outlet/>
            </main>
        </>
    )
}

export default AuthLayout