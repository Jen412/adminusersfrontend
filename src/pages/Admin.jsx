import { Link } from "react-router-dom";
import useUsuarios from "../hooks/useUsuarios";

const Admin = () => {
    
    const {usuarios} = useUsuarios();
    console.log(usuarios);
    return (
        
        <>
            <section className="container mx-auto">
                <h1 className="font-black text-gray-800 py-4 px-5 text-4xl text-center">Administra a los usarios</h1>
                <div className="flex justify-end">
                    <Link 
                        className="bg-sky-700 max-w-2xl mb-3 py-3 text-white uppercase font-bold rounded hover:cursor-pointer hover:bg-sky-800 transition-colors"
                        to=""
                    >Crear Usuario</Link>
                </div>
                
                <div className="w-full">
                    <table className="table">
                        <thead className="bg-sky-800 rounded-sm">
                            <tr>
                                <th className="w-1/4 py-2 px-2 text-white font-bold text-xl">Email</th>
                                <th className="w-1/4 py-2 px-2 text-white font-bold text-xl">Apellido Paterno</th>
                                <th className="w-1/4 py-2 px-2 text-white font-bold text-xl">Apellido Materno</th>
                                <th className="w-1/4 py-2 px-2 text-white font-bold text-xl">Telefono</th>
                            </tr> 
                        </thead>
                        <tbody>
                            {usuarios.length && 
                            usuarios.map(usuario => (
                                <tr key={usuario.id}>
                                    <td className="bg-gray-300 py-2 px-2 font-bold text-lg text-center">
                                        {usuario.email}    
                                    </td>    
                                    <td className="bg-gray-300 py-2 px-2 font-bold text-lg text-center">
                                        {usuario.apellidoP}    
                                    </td>    
                                    <td className="bg-gray-300 py-2 px-2 font-bold text-lg text-center">
                                        {usuario.apellidoM}    
                                    </td>    
                                    <td className="bg-gray-300 py-2 px-2 font-bold text-lg text-center">
                                        {usuario.telefono}    
                                    </td>    
                                </tr>    
                            ))}
                        </tbody>
                    </table>
                </div>
            </section>
        </>
    )
}

export default Admin