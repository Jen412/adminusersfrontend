import { Link, useNavigate} from "react-router-dom"

const Login = () => {
    const navigate = useNavigate();
    
    const handleSubmit = ()=>{
        navigate("/admin");
    }
    return (
        <>
            <h1 className="text-sky-600 font-black text-6xl capitalize">Inicia Sesión</h1>
            
            <form onSubmit={handleSubmit} className="my-10 bg-white shadow rounded-lg p-10">
                <div className="my-5">
                    <label className="text-gray-600 uppercase block text-xl font-bold" htmlFor="email">Email</label>
                    <input 
                        type="email" 
                        id="email"
                        placeholder="Email de Registro"
                        className="w-full mt-3 p-3 boder rounded-xl bg-gray-50"
                    />
                </div>
                <div className="my-5">
                    <label className="text-gray-600 uppercase block text-xl font-bold" htmlFor="password">Password</label>
                    <input 
                        type="password" 
                        id="password"
                        placeholder="Password de Registro"
                        className="w-full mt-3 p-3 boder rounded-xl bg-gray-50"
                    />
                </div>
                <input 
                    type="submit" 
                    value="Iniciar Sesión"
                    className="bg-sky-700 w-full mb-3 py-3 text-white uppercase font-bold rounded hover:cursor-pointer hover:bg-sky-800 transition-colors"
                />
            </form>

            <nav className="lg:flex lg:justify-between">
                <Link
                    className="block text-center my-5 text-slate-500 uppercase text-sm"
                    to="registrar"
                >¿No Tienes una Cuenta? Registrate</Link>

                <Link
                    className="block text-center my-5 text-slate-500 uppercase text-sm"
                    to="olvide-password"
                >Olvide Mi Password</Link>
            </nav>
        </>
    )
}

export default Login 